package com.example.checkmeapp.retrofit

import com.example.checkmeapp.model.Task
import com.example.checkmeapp.model.TaskList
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {

    @GET()
    suspend fun getLists(@Url url: String): Response<MutableList<TaskList>>

    @GET()
    suspend fun getTasks(@Url url: String): Response<MutableList<Task>>

    @POST("todolists")
    suspend fun addList(@Body taskList: TaskList): Response<TaskList>

    @POST("todolists/{idTaskList}/todoitems")
    suspend fun addTask(@Path("idTaskList") idList: Long, @Body task: Task): Response<Task>

    @PUT("todolists")
    suspend fun modifyList(@Body list: TaskList): Response<ResponseBody>

    @PUT("todolists/{idTaskList}/todoitems")
    suspend fun modifyTask(@Path("idTaskList") idList: Long, @Body task: Task): Response<ResponseBody>

    @DELETE("todolists/{idTaskList}")
    suspend fun deleteList(@Path("idTaskList") idList: Long): Response<ResponseBody>

    @DELETE("todolists/{idTaskList}/todoitems/{idItem}")
    suspend fun deleteTask(@Path("idTaskList") idList: Long, @Path("idItem") idTask: Long): Response<ResponseBody>


    companion object {
        val BASE_URL = "https://checkme-app.herokuapp.com/"

        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}
