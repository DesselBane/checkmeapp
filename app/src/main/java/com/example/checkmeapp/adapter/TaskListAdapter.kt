package com.example.checkmeapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.checkmeapp.listener.ListOnClickListener
import com.example.checkmeapp.R
import com.example.checkmeapp.databinding.ItemListBinding
import com.example.checkmeapp.model.TaskList

class TaskListAdapter(private val taskLists: MutableList<TaskList>,
                      private val listener: ListOnClickListener
):
    RecyclerView.Adapter<TaskListAdapter.ViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val taskList = taskLists[position]
        with(holder){
            setListener(taskList)
            binding.taskListName.text = taskList.name
        }
    }

    override fun getItemCount(): Int {
        return taskLists.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemListBinding.bind(view)

        fun setListener(taskList: TaskList){
            binding.root.setOnClickListener {
                listener.onClick(taskList)
            }
            binding.root.setOnLongClickListener {
                listener.onLongClick(taskList)
            }
        }
    }
}