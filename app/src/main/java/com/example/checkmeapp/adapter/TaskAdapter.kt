package com.example.checkmeapp.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.checkmeapp.R
import com.example.checkmeapp.listener.TaskOnClickListener
import com.example.checkmeapp.databinding.ItemTaskBinding
import com.example.checkmeapp.model.Task

class TaskAdapter(private val tasksList: MutableList<Task>,
                  private val listener: TaskOnClickListener
):
    RecyclerView.Adapter<TaskAdapter.ViewHolder>(){

    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_task, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasksList[position]
        with(holder){
            setListener(task)
            binding.taskName.text = task.name
            when (task.done){
                true -> {
                    binding.taskName.setTextColor(Color.parseColor("#B00020"))
                    binding.taskName.setTypeface(null, Typeface.ITALIC)
                    binding.taskName.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }
                else -> {
                    binding.taskName.setTextColor(Color.parseColor("#FF000000"))
                    binding.taskName.setTypeface(null, Typeface.NORMAL)
                    binding.taskName.paintFlags = binding.taskName.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                }
            }

        }
    }

    override fun getItemCount(): Int {
        return tasksList.size
    }

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemTaskBinding.bind(view)

        fun setListener(task: Task){
            binding.root.setOnClickListener {
                listener.onClick(task)
            }
            binding.root.setOnLongClickListener {
                listener.onLongClick(task)
            }
        }
    }
}