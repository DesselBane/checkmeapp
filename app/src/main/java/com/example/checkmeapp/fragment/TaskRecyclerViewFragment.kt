package com.example.checkmeapp.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.checkmeapp.R
import com.example.checkmeapp.listener.TaskOnClickListener
import com.example.checkmeapp.adapter.TaskAdapter
import com.example.checkmeapp.databinding.FragmentRecyclerViewTasksBinding
import com.example.checkmeapp.model.Task
import com.example.checkmeapp.model.TaskListViewModel
import java.util.*

class TaskRecyclerViewFragment : Fragment(), TaskOnClickListener {

    private lateinit var taskAdapter: TaskAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var binding: FragmentRecyclerViewTasksBinding

    private val model: TaskListViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewTasksBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.listName.text = model.actualList.value!!.name.uppercase(Locale.getDefault())

        model.tasks.value?.let {
            setUpRecyclerView(it)
        }

        model.tasks.observe(viewLifecycleOwner, Observer {
            if (model.tasks.value == null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else{
                setUpRecyclerView(it)
            }
        })

        binding.fabAdd.setOnClickListener {
            model.creatorOption = 2
            findNavController().navigate(R.id.action_taskRecyclerViewFragment_to_newTaskFragment)
        }

        binding.backArrow.setOnClickListener {
            findNavController().navigate(R.id.action_taskRecyclerViewFragment_to_taskListRecyclerViewFragment2)
        }
    }

    private fun setUpRecyclerView(tasks: MutableList<Task>) {
        taskAdapter = TaskAdapter(tasks, this)
        binding.recyclerView.apply{
            setHasFixedSize(true)
            linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            adapter = taskAdapter
        }
    }

    override fun onClick(task: Task) {
        when(task.done){
            true -> {
                var newTask = Task(task.idTask, task.name, task.idList, false)
                model.modifyTask(newTask)
            }
            false -> {
                var newTask = Task(task.idTask, task.name, task.idList, true)
                model.modifyTask(newTask)
            }
        }
    }

    override fun onLongClick(task: Task): Boolean {
        val dialog = Dialog(this.context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.fragment_dialog_menu)

        val edit = dialog.findViewById<Button>(R.id.edit)
        val delete = dialog.findViewById<Button>(R.id.delete)

        model.actualTask.value = task

        edit.setOnClickListener {
            model.creatorOption = 4
            findNavController().navigate(R.id.action_taskRecyclerViewFragment_to_newTaskFragment)
            dialog.dismiss()
        }
        delete.setOnClickListener {
            model.deleteTask(task)
            findNavController().navigate(R.id.action_taskRecyclerViewFragment_self)
            dialog.dismiss()
        }
        dialog.show()

        return true
    }

}