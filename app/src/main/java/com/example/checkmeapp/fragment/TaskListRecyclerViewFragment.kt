package com.example.checkmeapp.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.checkmeapp.listener.ListOnClickListener
import com.example.checkmeapp.R
import com.example.checkmeapp.adapter.TaskListAdapter
import com.example.checkmeapp.databinding.FragmentRecyclerViewListsBinding
import com.example.checkmeapp.model.TaskList
import com.example.checkmeapp.model.TaskListViewModel

class TaskListRecyclerViewFragment : Fragment(), ListOnClickListener {

    private lateinit var taskListAdapter: TaskListAdapter
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var binding: FragmentRecyclerViewListsBinding
    private val model: TaskListViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        binding = FragmentRecyclerViewListsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model.lists.value?.let{
            setUpRecyclerView(it)
        }

        model.lists.observe(viewLifecycleOwner, Observer {
            if (model.lists.value == null){
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else{
                setUpRecyclerView(it)
            }
        })

        binding.fabAdd.setOnClickListener {
            model.creatorOption = 1
            findNavController().navigate(R.id.action_taskListRecyclerViewFragment2_to_newTaskFragment)
        }
    }

    private fun setUpRecyclerView(
        list: MutableList<TaskList>, ) {
        taskListAdapter = TaskListAdapter(list, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            linearLayoutManager = LinearLayoutManager(context)
            layoutManager = linearLayoutManager
            adapter = taskListAdapter
        }
    }

    override fun onClick(taskList: TaskList){
        model.setTaskList(taskList)
        findNavController().navigate(R.id.action_taskListRecyclerViewFragment2_to_taskRecyclerViewFragment)
    }

    override fun onLongClick(taskList: TaskList): Boolean {
        val dialog = Dialog(this.context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.fragment_dialog_menu)

        dialog.findViewById<Button>(R.id.edit).setOnClickListener {
            model.creatorOption = 3
            findNavController().navigate(R.id.action_taskListRecyclerViewFragment2_to_newTaskFragment)
            dialog.dismiss()
        }
        dialog.findViewById<Button>(R.id.delete).setOnClickListener {
            model.deleteList(taskList)
            findNavController().navigate(R.id.action_taskListRecyclerViewFragment2_self)
            dialog.dismiss()
        }
        dialog.show()

        return true
    }


}