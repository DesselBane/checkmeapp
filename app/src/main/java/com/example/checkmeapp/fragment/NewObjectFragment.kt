package com.example.checkmeapp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.checkmeapp.R
import com.example.checkmeapp.databinding.FragmentNewObjectBinding
import com.example.checkmeapp.model.Task
import com.example.checkmeapp.model.TaskList
import com.example.checkmeapp.model.TaskListViewModel

class NewObjectFragment : Fragment() {

    lateinit var binding: FragmentNewObjectBinding
    private val model: TaskListViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewObjectBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (model.creatorOption) {
            1 -> {
                binding.buttonAdd.setOnClickListener {
                    val editText = binding.edittextName
                    val newList = TaskList(0, editText.text.toString())
                    model.addList(newList)
                    findNavController().navigate(R.id.action_newTaskFragment_to_taskListRecyclerViewFragment2)
                }
            }
            2 -> {
                binding.buttonAdd.setOnClickListener {
                    val editText = binding.edittextName
                    val newTask = Task(0, editText.text.toString(), model.actualList.value!!, false)
                    model.addTask(newTask)
                    findNavController().navigate(R.id.action_newTaskFragment_to_taskRecyclerViewFragment)
                }
            }
            3 -> {
                binding.buttonAdd.text = getString(R.string.edit)
                binding.buttonAdd.setOnClickListener {
                    val newText = binding.edittextName
                    val newList = TaskList(model.actualList.value!!.idList, newText.text.toString())
                    model.modifyList(newList)
                    findNavController().navigate(R.id.action_newTaskFragment_to_taskListRecyclerViewFragment2)
                }
            }
            4 -> {
                binding.buttonAdd.text = getString(R.string.edit)
                binding.buttonAdd.setOnClickListener {
                    val newText = binding.edittextName
                    val newTask = Task(model.actualTask.value!!.idTask, newText.text.toString(), model.actualList.value!!, model.actualTask.value!!.done )
                    model.modifyTask(newTask)
                    findNavController().navigate(R.id.action_newTaskFragment_to_taskRecyclerViewFragment)
                }
            }
        }
    }
}