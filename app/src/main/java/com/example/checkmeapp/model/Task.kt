package com.example.checkmeapp.model

class Task(val idTask: Long, var name: String, val idList: TaskList, val done: Boolean)