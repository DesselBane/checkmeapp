package com.example.checkmeapp.model

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.checkmeapp.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TaskListViewModel: ViewModel() {
    val repository = Repository()
    var lists = MutableLiveData<MutableList<TaskList>>()
    var tasks = MutableLiveData<MutableList<Task>>()
    var actualList  = MutableLiveData<TaskList>()
    var actualTask = MutableLiveData<Task>()
    var creatorOption: Int = 0

    init{
        fetchLists("todolists")
    }

    fun fetchLists(url:String) {
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.fetchLists(url) }
            if (response.isSuccessful) {
                lists.postValue(response.body())
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun fetchTasks(url: String){
        val emptyTasks = mutableListOf<Task>() // Borra los items de listas anteriores
        tasks.postValue(emptyTasks)
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.fetchTasks(url)}
            if (response.isSuccessful) {
                tasks.postValue(response.body())
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun setTaskList(list: TaskList){
        actualList.value = list
        fetchTasks("todolists/"+actualList.value!!.idList+"/todoitems")
    }



    fun addList(taskList: TaskList){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.pushList(taskList)}
            if (response.isSuccessful) {
                creatorOption = 1
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun addTask(task: Task){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.pushTask(actualList.value!!, task)}
            if (response.isSuccessful) {
                creatorOption = 2
                setTaskList(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun modifyList(list: TaskList){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.modifyList(list)}
            if (response.isSuccessful) {
                creatorOption = 3
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun modifyTask(task: Task){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.modifyTask(actualList.value!!, task)}
            if (response.isSuccessful) {
                creatorOption = 4
                setTaskList(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun deleteList(list: TaskList){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.deleteList(list)}
            if (response.isSuccessful) {
                fetchLists("todolists")
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }

    fun deleteTask(task: Task){
        viewModelScope.launch {
            val response = withContext(Dispatchers.IO) { repository.deleteTask(actualList.value!!, task)}
            if (response.isSuccessful) {
                setTaskList(actualList.value!!)
            } else {
                Log.e("ERROR", response.message())
            }
        }
    }
}