package com.example.checkmeapp.listener

import com.example.checkmeapp.model.TaskList

interface ListOnClickListener {
    fun onClick(taskList: TaskList)
    fun onLongClick(taskList: TaskList): Boolean
}