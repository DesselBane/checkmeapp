package com.example.checkmeapp.listener

import com.example.checkmeapp.model.Task

interface TaskOnClickListener {
    fun onClick(task: Task)
    fun onLongClick(task: Task): Boolean
}