package com.example.checkmeapp

import com.example.checkmeapp.model.Task
import com.example.checkmeapp.model.TaskList
import com.example.checkmeapp.retrofit.ApiInterface

class Repository {

    private val apiInterface = ApiInterface.create()

    suspend fun fetchLists(url: String) = apiInterface.getLists(url)

    suspend fun fetchTasks(url: String) = apiInterface.getTasks(url)

    suspend fun pushList(taskList: TaskList) = apiInterface.addList(taskList)

    suspend fun pushTask(list: TaskList, task: Task) = apiInterface.addTask(list.idList, task)

    suspend fun modifyList(list: TaskList) = apiInterface.modifyList(list)

    suspend fun modifyTask(list: TaskList, task: Task) = apiInterface.modifyTask(list.idList, task)

    suspend fun deleteList(list: TaskList) = apiInterface.deleteList(list.idList)

    suspend fun deleteTask(list: TaskList, task: Task) = apiInterface.deleteTask(list.idList, task.idTask)
}
