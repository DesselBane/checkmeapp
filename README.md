# CheckMeApp


## Una aplicació per guardar llistes de cosas a fer

Aquesta apliació serveix per emmagatzemar llistes que contenen ítems o tasques que volem fer o tenim pendents. Podem afegir llistes de tasques, deures, de la compra o qualsevol tipus de informació que ens quedi pendent a fer i no volem oblidar.


## Ús de l'aplicaió

L'ús de *CheckMe* és molt senzill. Ens trobem amb una pantalla que ens indica on es troben les llistes. Per afegir una nova hem de fer clic a l'icone **+** que es troba a la part inferior dreta. Una vegada creada la llista podrem editarla o eliminarla fent un clic llarg a la llista desitjada.

Quan fem clic al nom d'una llista, es mostraràn els items que estiguin dins d'ella. Si no existeix cap, podem crear nous seguint els mateixos passos que amb les llistes.

Si fem clic a un item d'una llista, aquest es marcarà com a fet o al revès.

Per tornar al menú de les llistes només hem de fer clic a la fletxa que es troba a la part inferior esquerra.


## Més informació

Com aquest aplicació és el resultat de hores de treball per al projecte que hem de realitzar per M13, en aquest apartat introduirem informació adicional a la realització del treball.

### Histories d'usuari

Segons hem programat la nostra aplicació, hem estat capaços de cumplir amb les següents històries d'usuari:

- H1. Crear una llista
- H2. Eliminar una llista
- H3. Modificar una llista (el seu nom)
- H4. Mostrar llistes
- H5. Consultar tots els items d'una llista
- H6. Consulta un ítem per id *
- H7. Afegir ítems a la llista
- H8. Marcar / desmarcar itém de la llista
- H9. Elminar ítem de la llista

*Aquesta historia no és possible compobarla a simple vista.


### Estil de l'aplicació

L'aplicació és molt senzilla i fàcil d'utilitzar. Té una base predefinida que té un mateix fons a totes les pantalles i permet una navegació fluida i sense talls bruscos a la vista.
